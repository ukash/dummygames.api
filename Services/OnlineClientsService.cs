using System.Collections.Concurrent;
using System.Linq;
using DummyGames.API.Dtos;

namespace DummyGames.API.Services
{
	public class OnlineClientsService : IOnlineClientsService
	{
		private readonly ConcurrentDictionary<string, string> connections = new ConcurrentDictionary<string, string>();
		public OnlineUsersDto OnlineUsers => new OnlineUsersDto
		{
			userIds = connections.Values.Where(v => v != null).Distinct().ToList().ConvertAll(int.Parse),
			Guests = connections.Values.Where(v => v == null).Count()
		};

		public void AddClient(string connectionId, string userIdentifier)
			=> connections.TryAdd(connectionId, userIdentifier);

		public void RemoveClient(string connectionId)
			=> connections.TryRemove(connectionId, out _);
	}
}