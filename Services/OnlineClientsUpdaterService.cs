using System;
using System.Threading;
using System.Threading.Tasks;
using DummyGames.API.Data;
using DummyGames.API.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace DummyGames.API.Services
{
	public class OnlineClientsUpdaterService : IHostedService, IDisposable
	{
		private readonly int interval;
		private readonly IOnlineClientsService onlineClientsService;
		private readonly IHubContext<ChatHub, IChatClient> chatContext;
		private Timer timer;

		public OnlineClientsUpdaterService(IConfiguration config, IOnlineClientsService onlineClientsService,
			IHubContext<ChatHub, IChatClient> chatContext)
		{
			this.onlineClientsService = onlineClientsService;
			this.chatContext = chatContext;
			interval = config.GetValue<int>("AppSettings:Chat:UpdateInterval", 15);
		}

		public void Dispose()
		{
			timer?.Dispose();
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			timer = new Timer(DoWork, null, TimeSpan.Zero,
				TimeSpan.FromSeconds(interval));

			return Task.CompletedTask;
		}

		private void DoWork(object state)
		{
			if (onlineClientsService.OnlineUsers.Total > 0)
				chatContext.Clients.All.ReceiveOnlineUsers(onlineClientsService.OnlineUsers);
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			timer?.Change(Timeout.Infinite, 0);

			return Task.CompletedTask;
		}
	}
}