using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DummyGames.API.Data;
using DummyGames.API.Dtos;
using DummyGames.API.Hubs;
using DummyGames.API.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DummyGames.API.Services
{
	public class GameService : IGameService
	{
		public IEnumerable<int> InGamePlayers => throw new System.NotImplementedException();
		private readonly ConcurrentDictionary<int, GameRequest> gameRequests = new ConcurrentDictionary<int, GameRequest>();
		private readonly ConcurrentDictionary<int, Game> games = new ConcurrentDictionary<int, Game>();
		private readonly IServiceProvider serviceProvider;
		private readonly IHubContext<GameHub, IGameClient> gameContext;
		private readonly IPointsService pointsService;
		private readonly int timeOut, turnDuration;

		public GameService(IServiceProvider serviceProvider, IConfiguration config,
			IHubContext<GameHub, IGameClient> gameContext, IPointsService pointsService)
		{
			timeOut = config.GetValue<int>("AppSettings:Game:ResponseTimeOut", 60);
			turnDuration = config.GetValue<int>("AppSettings:Game:TicTacToe:TurnDuration", 45);


			this.serviceProvider = serviceProvider;
			this.gameContext = gameContext;
			this.pointsService = pointsService;
		}

		public IEnumerable<GameRequest> InProgressGameRequests => gameRequests.Values;
		public IEnumerable<Game> InProgressGames => games.Values;

		public void AddGameRequest(GameRequest gameRequest)
		{
			gameRequests.TryAdd(gameRequest.Id, gameRequest);
			//	TODO: Add some kind of logging
			TimeOutGameRequest(gameRequest.Id)
				.ContinueWith(t => throw t.Exception,
			TaskContinuationOptions.OnlyOnFaulted);
		}

		public async Task<GameRequest> UpdateInProgressGameRequest(int id, GameRequestStatus status)
		{
			if (!gameRequests.ContainsKey(id))
				throw new Exception("Failed to find the game request.");

			if (!gameRequests.TryRemove(id, out var removedGameRequest))
				throw new Exception("Failed to remove game request.");

			removedGameRequest.Status = status;

			if (status == GameRequestStatus.Accepted) await AddNewGame(removedGameRequest);

			return removedGameRequest;
		}

		private async Task AddNewGame(GameRequest gameRequest)
		{
			var game = CreateGameObject(gameRequest);

			try
			{
				await pointsService.RemovePoints(game.Players.ToArray(), gameRequest.Bid);
			}
			catch
			{
				throw new Exception("Failed to gather bids from players.");
			}

			using (var scope = serviceProvider.CreateScope())
			{
				var repo = scope.ServiceProvider
				  .GetRequiredService<IRepository>();

				repo.Add(game);
				if (!await repo.SaveAll())
					throw new Exception("Failed to save a new game.");
			}

			games.TryAdd(game.Id, game);
			await BroadcastGameState(game);

			if (game is ITurnBasedGame turnBasedGame)
				TimeOutGameMove(game.Id, turnBasedGame.TurnEndAt);
		}

		public async Task FinishGame(Game game)
		{
			if (game.Status == GameStatus.InProgress)
				throw new Exception("The game is not yet over.");
			try
			{
				await pointsService.DistributePoints(game.Players, game.WinnerId, game.Pot);
				games.TryRemove(game.Id, out _);
			}
			catch (System.Exception e)
			{
				//	TODO: Add logging when ppl don't get points.
				throw e;
			}
		}

		private Game GetGame(int id) => InProgressGames.SingleOrDefault(g => g.Id == id);

		public async Task BroadcastGameState(int id)
		{
			var game = GetGame(id) ?? throw new Exception("This game is no longer valid.");
			await BroadcastGameState(game);
		}

		public async Task BroadcastGameState(Game game)
		{
			using (var scope = serviceProvider.CreateScope())
			{
				var mapper = scope.ServiceProvider
				  .GetRequiredService<IMapper>();

				var gameStateToReturn = mapper.Map<GameStateDto>(game);
				await gameContext.Clients.Users(game.Players.Select(p => p.Id.ToString()).ToList())
					.ReceiveGameState(gameStateToReturn);
			}
		}

		private Game CreateGameObject(GameRequest gameRequest)
		{
			switch (gameRequest.GameType)
			{
				case GameType.TicTacToe:
					return new TicTacToe(gameRequest, turnDuration);
				default:
					return null;
			}
		}

		private async Task<bool> UpdateGameRequest(GameRequest gameRequest, GameRequestStatus status)
		{
			gameRequest.Status = status;
			gameRequest.DateCompleted = DateTime.Now;

			using (var scope = serviceProvider.CreateScope())
			{
				var repo = scope.ServiceProvider
				  .GetRequiredService<IRepository>();

				repo.Update(gameRequest);

				return await repo.SaveAll();
			}
		}

		public async Task GameMove(int id, dynamic move)
		{
			if (!(GetGame(id) is Game game))
				throw new Exception("This game is no longer available.");

			switch (game.GameType)
			{
				case GameType.TicTacToe:
					var theGame = game as TicTacToe;

					theGame.Move(move);

					if (theGame.Status != GameStatus.InProgress)
						await FinishGame(theGame);
					else
						TimeOutGameMove(theGame.Id, theGame.TurnEndAt);

					await BroadcastGameState(game);

					break;
				default:
					throw new Exception("Invalid game type.");
			}
		}

		public bool IsUserTurn(int userId, int gameId)
		{
			if (!(GetGame(gameId) is ITurnBasedGame game))
				throw new Exception("This game is no longer available or it is not a turn-based game.");

			return game.CurrentPlayerId == userId;
		}

		public async void TimeOutGameMove(int id, DateTime? turnEndAt)
		{
			await Task.Delay(TimeSpan.FromSeconds(turnDuration));

			if (GetGame(id) is ITurnBasedGame game)
			{
				if (game.TurnEndAt == turnEndAt)
				{
					try
					{
						game.NextPlayer();
						TimeOutGameMove(id, game.TurnEndAt);
					}
					catch { }

					await BroadcastGameState(id);
				}
			}
		}

		private async Task TimeOutGameRequest(int id)
		{
			await Task.Delay(TimeSpan.FromSeconds(timeOut));

			if (gameRequests.ContainsKey(id))
			{
				if (!gameRequests.TryRemove(id, out var removedGameRequest))
					throw new Exception("Failed to remove timed out game request.");

				if (!await UpdateGameRequest(removedGameRequest, GameRequestStatus.TimedOut))
					throw new Exception("Failed to update timed out game request.");

				using (var scope = serviceProvider.CreateScope())
				{
					var mapper = scope.ServiceProvider
						  .GetRequiredService<IMapper>();

					var gameRequestToReturn = mapper.Map<GameRequestDto>(removedGameRequest);

					await gameContext.Clients.Users(removedGameRequest.SenderId.ToString(), removedGameRequest.RecipientId.ToString())
						.ReceiveChallenge(gameRequestToReturn);
				}
			}
		}
	}
}