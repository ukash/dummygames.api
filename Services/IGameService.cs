using System.Collections.Generic;
using System.Threading.Tasks;
using DummyGames.API.Models;

namespace DummyGames.API.Services
{
	public interface IGameService
	{
		IEnumerable<int> InGamePlayers { get; }
		IEnumerable<GameRequest> InProgressGameRequests { get; }
		void AddGameRequest(GameRequest gameRequest);
		Task<GameRequest> UpdateInProgressGameRequest(int id, GameRequestStatus status);
		IEnumerable<Game> InProgressGames { get; }
		Task FinishGame(Game game);
		bool IsUserTurn(int userId, int gameId);
		Task GameMove(int id, dynamic move);
	}
}