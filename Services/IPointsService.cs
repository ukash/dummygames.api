using System.Threading.Tasks;
using DummyGames.API.Data;

namespace DummyGames.API.Services
{
	public interface IPointsService
	{
		Task AddPoints(int userId, int count);
		Task RemovePoints(int userId, int count);
		Task RemovePoints(User[] users, int count);
		Task DistributePoints(User[] users, int? winnerId, int pot);
	}
}