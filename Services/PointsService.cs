using System;
using System.Threading.Tasks;
using DummyGames.API.Data;
using Microsoft.Extensions.DependencyInjection;

namespace DummyGames.API.Services
{
	public class PointsService : IPointsService
	{
		private readonly IServiceProvider serviceProvider;

		public PointsService(IServiceProvider serviceProvider)
		{
			this.serviceProvider = serviceProvider;
		}

		public async Task AddPoints(int userId, int count)
		{
			using (var scope = serviceProvider.CreateScope())
			{
				var repo = scope.ServiceProvider.GetRequiredService<IRepository>();

				if (!(await repo.GetUser(userId) is User user))
					throw new Exception("Failed to find the user.");

				user.Balance += count;

				repo.Update(user);

				if (!await repo.SaveAll())
					throw new Exception("Failed to update user balance.");
			}
		}

		public async Task RemovePoints(int userId, int count)
		{
			using (var scope = serviceProvider.CreateScope())
			{
				var repo = scope.ServiceProvider.GetRequiredService<IRepository>();

				if (!(await repo.GetUser(userId) is User user))
					throw new Exception("Failed to find the user.");

				if (user.Balance - count < 0)
					throw new Exception("User balance is not sufficient to perform this operation.");

				user.Balance -= count;

				repo.Update(user);

				if (!await repo.SaveAll())
					throw new Exception("Failed to update user balance.");
			}
		}

		public async Task DistributePoints(User[] users, int? winnerId, int pot)
		{
			if (winnerId != null)
				await AddPoints(winnerId.Value, pot);
			else
			{
				var bid = pot / users.Length;
				foreach (var user in users) await AddPoints(user.Id, bid);
			}
		}
		public async Task RemovePoints(User[] users, int count)
		{
			foreach (var user in users) { await RemovePoints(user.Id, count); }
		}
	}
}