using System.Collections.Generic;
using System.Threading.Tasks;
using DummyGames.API.Dtos;
using DummyGames.API.Models;

namespace DummyGames.API.Services
{
	public interface IChatService
	{
		IEnumerable<ChatMessage> Messages { get; }
		void AddMessage(ChatMessage message);
		Task LoadMessagesFromDatabase();
	}
}