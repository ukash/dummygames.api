using System.Threading.Tasks;
using DummyGames.API.Dtos;

namespace DummyGames.API.Services
{
	public interface IOnlineClientsService
	{
		OnlineUsersDto OnlineUsers { get; }
		void AddClient(string connectionId, string userIdentifier);
		void RemoveClient(string connectionId);
	}
}