using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DummyGames.API.Dtos;
using DummyGames.API.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Concurrent;
using System.Linq;
using Microsoft.AspNetCore.SignalR;
using DummyGames.API.Hubs;
using System.Threading;
using DummyGames.API.Data;

namespace DummyGames.API.Services
{
	public class ChatService : IChatService
	{
		private readonly IServiceProvider serviceProvider;
		private readonly int messagesCount;
		private readonly ConcurrentQueue<ChatMessage> messages = new ConcurrentQueue<ChatMessage>();
		public IEnumerable<ChatMessage> Messages => messages;

		public ChatService(IServiceProvider serviceProvider, IConfiguration config)
		{
			messagesCount = config.GetValue<int>("AppSettings:Chat:MessagesCount", 20);

			this.serviceProvider = serviceProvider;
		}

		public async Task LoadMessagesFromDatabase()
		{
			using (var scope = serviceProvider.CreateScope())
			{
				var repo = scope.ServiceProvider
				  .GetRequiredService<IRepository>();

				var messagesFromDatabase = await repo.GetGlobalChatMessages(messagesCount);
				foreach (var message in messagesFromDatabase) messages.Enqueue(message);
			}
		}
		
		public void AddMessage(ChatMessage message)
		{
			messages.Enqueue(message);
			if (messages.Count > messagesCount)
				messages.TryDequeue(out _);
		}
	}
}