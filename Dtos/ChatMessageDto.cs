using System;

namespace DummyGames.API.Dtos
{
	public class ChatMessageDto
	{
		public int Id { get; set; }
		public string SenderUserName { get; set; }
		public string SenderAvatarUrl { get; set; }
		public DateTime DateSent { get; set; }
		public string Content { get; set; }
	}
}