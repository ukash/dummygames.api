using System;
using System.Collections.Generic;
using DummyGames.API.Models;

namespace DummyGames.API.Dtos
{
	public class UserForDetailedDto
	{
		public int Id { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string AvatarUrl { get; set; }
		public int Balance { get; set; }
		public DateTime Created { get; set; }
		public DateTime LastActive { get; set; }
		public ICollection<UserRole> UserRoles { get; set; }
	}
}