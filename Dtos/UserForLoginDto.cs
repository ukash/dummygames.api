using System.ComponentModel.DataAnnotations;

namespace DummyGames.API.Dtos
{
	public class UserForLoginDto
	{
		[Required]
		public string Identifier { get; set; }
		[Required]
		public string Password { get; set; }
	}
}