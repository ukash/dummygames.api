using System.Collections.Generic;
using System.Linq;

namespace DummyGames.API.Dtos
{
	public class OnlineUsersDto
	{
		public IEnumerable<int> userIds { get; set; }
		public int Guests { get; set; }
		public int Total => userIds.Count() + Guests;
	}
}