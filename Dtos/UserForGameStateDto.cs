namespace DummyGames.API.Dtos
{
	public class UserForGameStateDto
	{
		public int Id { get; set; }
		public string UserName { get; set; }
		public string AvatarUrl { get; set; }
	}
}