using System;
using DummyGames.API.Models;

namespace DummyGames.API.Dtos
{
	public class GameRequestDto
	{
		public int Id { get; set; }
		public int SenderId { get; set; }
		public string SenderUserName { get; set; }
		public string SenderAvatarUrl { get; set; }
		public int RecipientId { get; set; }
		public string RecipientUserName { get; set; }
		public string RecipientAvatarUrl { get; set; }
		public DateTime DateSent { get; set; }
		public int Bid { get; set; }
		public GameType GameType { get; set; }
		public GameRequestStatus Status { get; set; }
	}
}