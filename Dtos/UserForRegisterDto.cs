using System;
using System.ComponentModel.DataAnnotations;

namespace DummyGames.API.Dtos
{
	public class UserForRegisterDto
	{
		[Required]
		public string UserName { get; set; }
		[Required]
		[StringLength(24, MinimumLength = 5, ErrorMessage = "You must specify password between 5 and 24 characters.")]
        public string Password { get; set; }
		[Required]
        [EmailAddress]
        public string Email { get; set; }
		public DateTime Created { get; set; } = DateTime.Now;
		public DateTime LastActive { get; set; } = DateTime.Now;
	}
}