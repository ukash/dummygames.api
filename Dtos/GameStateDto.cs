using System;
using DummyGames.API.Models;

namespace DummyGames.API.Dtos
{
	public class GameStateDto
	{
		public int Id { get; set; }
		public UserForGameStateDto[] Players { get; private set; }
		public GameStatus Status { get; private set; }
		public GameType GameType { get; set; }
		public int Pot { get; private set; }
		public int? WinnerId { get; set; }
		public dynamic State { get; set; }
		public int CurrentPlayerId { get; set; }
		public DateTime TurnEndAt { get; set; }

	}
}