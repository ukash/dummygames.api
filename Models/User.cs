using System;
using System.Collections.Generic;
using DummyGames.API.Models;
using Microsoft.AspNetCore.Identity;

namespace DummyGames.API.Data
{
	public class User : IdentityUser<int>
	{
		public int Balance { get; set; }
		public string AvatarUrl { get; set; }
		public DateTime Created { get; set; }
		public DateTime LastActive { get; set; }
		public ICollection<UserGame> UserGames { get; set; } 
		public ICollection<UserRole> UserRoles { get; set; }
	}
}