using System;
using DummyGames.API.Data;

namespace DummyGames.API.Models
{
	public class ChatMessage
	{
		public int Id { get; set; }
		public int SenderId { get; set; }
		public User Sender { get; set; }
		public DateTime DateSent { get; set; } = DateTime.Now;
		public string Content { get; set; }

		public ChatMessage(User sender, string message)
		{
			SenderId = sender.Id;
			Sender = sender;
			Content = message;
		}
		public ChatMessage() {}
	}
}