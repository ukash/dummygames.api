using System;
using DummyGames.API.Data;

namespace DummyGames.API.Models
{
	public class GameRequest
	{
		public int Id { get; set; }
		public int SenderId { get; set; }
		public User Sender { get; set; }
		public int RecipientId { get; set; }
		public User Recipient { get; set; }
		public DateTime DateSent { get; set; } = DateTime.Now;
		public DateTime DateCompleted { get; set; }
		public int Bid { get; set; }
		public GameType GameType { get; set; }
		public GameRequestStatus Status { get; set; } = GameRequestStatus.InProgress;

		public GameRequest(User sender, User recipient, int bid, GameType gameType)
		{
			SenderId = sender.Id;
			Sender = sender;
			RecipientId = recipient.Id;
			Recipient = recipient;
			Bid = bid;
			GameType = gameType;
		}

		public GameRequest(){}
	}
	
	public enum GameRequestStatus
	{
		InProgress,
		Rejected,
		Accepted,
		TimedOut,
		Cancelled,
		Closed
	}
}