using System;

namespace DummyGames.API.Models
{
	public interface ITurnBasedGame<TState, TMove> : ITurnBasedGame
	{
		TState State { get; }
		void Move(TMove move);
	}
	public interface ITurnBasedGame
	{
		int CurrentPlayerId { get; }
		DateTime? TurnEndAt { get; }
		void Move(dynamic move);
		void NextPlayer();
	}
}