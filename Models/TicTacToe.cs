using System;
using System.Drawing;

namespace DummyGames.API.Models
{
	public class TicTacToe : Game, ITurnBasedGame<bool?[,], Point>
	{
		private const int fieldSize = 3;
		private readonly int turnDuration;
		private readonly bool?[,] board = new bool?[fieldSize, fieldSize];
		private DateTime? turnEndAt;
		private int turnNumber;

		public TicTacToe() { }

		public TicTacToe(GameRequest gameRequest, int turnDuration) : base(gameRequest)
		{
			this.turnDuration = turnDuration;
			turnEndAt = DateTime.Now.AddSeconds(turnDuration);
		}

		public int CurrentPlayerId => Players[turnNumber % Players.Length].Id;
		public DateTime? TurnEndAt => turnEndAt;
		public bool?[,] State => board;
		public void Move(Point move)
		{
			if (Status != GameStatus.InProgress)
				throw new Exception("This game is already finished.");

			if (move.X < 0 || move.X > fieldSize || move.Y < 0 || move.Y > fieldSize)
				throw new ArgumentOutOfRangeException($"This position is outside of the {fieldSize}x{fieldSize} field.");

			if (board[move.X, move.Y] != null)
				throw new Exception("Invalid move. This position has been already selected.");

			board[move.X, move.Y] = Convert.ToBoolean(turnNumber % 2);

			if (hasWinner())
				GameOver(CurrentPlayerId);
			else if (turnNumber < 8)
			{
				turnNumber++;
				turnEndAt = DateTime.Now.AddSeconds(turnDuration);
			}
			else
				GameOver(null);
		}
		public void Move(dynamic move) => Move(new Point((int)move["x"], (int)move["y"]));

		public void NextPlayer()
		{
			if (Status != GameStatus.InProgress)
				throw new Exception("This game is already finished.");

			var rng = new Random();

			var failed = true;

			while (failed)
			{
				try
				{
					Move(new Point(rng.Next(3), rng.Next(3)));
					failed = false;
				}
				catch { }
			}
			turnEndAt = DateTime.Now.AddSeconds(turnDuration);
		}
		private bool hasWinner()
		{
			if (turnNumber < 4) return false;

			for (int i = 0; i < 3; i++)
			{
				if (board[i, 0] != null && board[i, 0] == board[i, 1] && board[i, 1] == board[i, 2])
					return true;

				if (board[0, 1] != null && board[0, i] == board[1, i] && board[1, i] == board[2, i])
					return true;
			}

			if (board[1, 1] != null && (board[0, 0] == board[1, 1] && board[1, 1] == board[2, 2] ||
				board[0, 2] == board[1, 1] && board[1, 1] == board[2, 0]))
				return true;

			return false;
		}
	}
}