using System;
using DummyGames.API.Models;

namespace DummyGames.API.Data
{
	public class Match
	{
		public int Id { get; set; }
		public User[] Players { get; set; }
		public int Pot { get; set; }
		public GameType GameType { get; set; }
		public GameStatus Status { get; set; } = GameStatus.InProgress;
		public int? WinnerId { get; set; } = null;
		public DateTime StartedAt { get; set; } = DateTime.Now;
		public DateTime? FinishedAt { get; set; } = null;
	}
}