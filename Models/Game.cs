using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DummyGames.API.Data;

namespace DummyGames.API.Models
{
	public abstract class Game
	{
		public int Id { get; set; }
		public ICollection<UserGame> UserGames { get; private set; }
		public GameStatus Status { get; private set; }
		public GameType GameType { get; set; }
		public int Pot { get; private set; }
		public int? WinnerId { get; set; }
		public User Winner { get; set; }
		public int GameRequestId { get; set; }
		public DateTime StartedAt { get; set; }
		public DateTime FinishedAt { get; set; }
		[NotMapped]
		public User[] Players { get; set; }

		protected Game(GameRequest gameRequest)
		{
			Players = new[] { gameRequest.Sender, gameRequest.Recipient };
			Status = GameStatus.InProgress;
			GameType = gameRequest.GameType;
			Pot = gameRequest.Bid * Players.Length;
			StartedAt = DateTime.Now;
			GameRequestId = gameRequest.Id;

			UserGames = new List<UserGame> {
				new UserGame { GameId = Id, UserId = gameRequest.SenderId },
				new UserGame { GameId = Id, UserId = gameRequest.RecipientId }
			};
		}

		public Game() { }

		public void AddPlayers(User sender, User recipient)
		{

			var player1 = new UserGame { Game = this, User = recipient };
			var player2 = new UserGame { Game = this, User = sender };

			UserGames.Add(player1);
			UserGames.Add(player2);
		}

		protected void GameOver(int? winnerId)
		{
			Status = GameStatus.Finished;
			FinishedAt = DateTime.Now;

			if (winnerId != null)
			{
				WinnerId = winnerId;
				Winner = Players.SingleOrDefault(p => p.Id == winnerId);
			}
		}

		public void CancelGame()
		{
			Status = GameStatus.Cancelled;
			FinishedAt = DateTime.Now;
		}
	}
	public enum GameType
	{
		TicTacToe
	}
	public enum GameStatus
	{
		InProgress,
		Finished,
		Cancelled
	}
}