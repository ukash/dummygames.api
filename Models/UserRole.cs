using DummyGames.API.Data;
using Microsoft.AspNetCore.Identity;

namespace DummyGames.API.Models
{
    public class UserRole : IdentityUserRole<int>
    {
        public User User { get; set; }
        public Role Role { get; set; }
    }
}