using DummyGames.API.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DummyGames.API.Data
{
	public class DataContext : IdentityDbContext<User, Role, int,
		IdentityUserClaim<int>, UserRole, IdentityUserLogin<int>,
		IdentityRoleClaim<int>, IdentityUserToken<int>>
	{
		public DataContext(DbContextOptions<DataContext> options) : base(options) { }

		public DbSet<ChatMessage> GlobalChatMessages { get; set; }
		public DbSet<GameRequest> GameRequests { get; set; }
		public DbSet<Game> Games { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			builder.Entity<UserRole>(userRole =>
			{
				userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

				userRole.HasOne(ur => ur.Role)
					.WithMany(r => r.UserRoles)
					.HasForeignKey(ur => ur.RoleId)
					.IsRequired();

				userRole.HasOne(ur => ur.User)
					.WithMany(r => r.UserRoles)
					.HasForeignKey(ur => ur.UserId)
					.IsRequired();
			});

			builder.Entity<UserGame>(userGame =>
			{
				userGame.HasKey(ug => new { ug.UserId, ug.GameId });

				userGame.HasOne(ug => ug.Game)
					.WithMany(g => g.UserGames)
					.HasForeignKey(ug => ug.GameId)
					.IsRequired();

				userGame.HasOne(ug => ug.User)
					.WithMany(u => u.UserGames)
					.HasForeignKey(ug => ug.UserId)
					.IsRequired();
			});

			builder.Entity<TicTacToe>().HasBaseType<Game>();
		}
	}
}