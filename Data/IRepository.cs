using System.Collections.Generic;
using System.Threading.Tasks;
using DummyGames.API.Models;

namespace DummyGames.API.Data
{
	public interface IRepository
	{
		void Add<T>(T entity) where T : class;
		void Delete<T>(T entity) where T : class;
		void Update<T>(T entity) where T : class;

		Task<bool> SaveAll();
		Task<User> GetUser(int id);
		Task<PagedList<User>> GetUsers(UserParams userParams);
		Task<ICollection<ChatMessage>> GetGlobalChatMessages(int count);
		Task<GameRequest> GetGameRequest(int id);
	}
}