using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DummyGames.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DummyGames.API.Data
{
	public class Repository : IRepository
	{
		private readonly DataContext context;

		public Repository(DataContext context)
		{
			this.context = context;
		}

		public void Add<T>(T entity) where T : class
		{
			context.Add(entity);
		}

		public void Delete<T>(T entity) where T : class
		{
			context.Remove(entity);
		}

		public void Update<T>(T entity) where T : class
		{
			context.Update(entity);
		}

		public async Task<GameRequest> GetGameRequest(int id)
		{
			return await context.GameRequests.FirstOrDefaultAsync(r => r.Id == id);
		}

		public async Task<ICollection<ChatMessage>> GetGlobalChatMessages(int count)
		{
			return await context.GlobalChatMessages
				.Include(m => m.Sender)
				.OrderByDescending(u => u.DateSent)
				.Take(count)
				.ToListAsync();
		}

		public async Task<User> GetUser(int id)
		{
			return await context.Users.FirstOrDefaultAsync(u => u.Id == id);
		}

		public async Task<PagedList<User>> GetUsers(UserParams userParams)
		{
			var users = context.Users
				.Include(u => u.UserRoles)
				.AsQueryable();

			if (userParams.userIds != null)
			{
				userParams.userIds.Remove(userParams.SenderId);
				users.Where(u => userParams.userIds.Contains(u.Id));
			}

			switch (userParams.OrderBy)
			{
				case "created":
					users = users.OrderByDescending(u => u.Created);
					break;

				case "balance":
					users = users.OrderByDescending(u => u.Balance);
					break;

				default:
					users = users.OrderByDescending(u => u.LastActive);
					break;
			}

			return await PagedList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
		}

		public async Task<bool> SaveAll()
		{
			return await context.SaveChangesAsync() > 0;
		}
	}
}