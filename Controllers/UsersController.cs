using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DummyGames.API.Data;
using DummyGames.API.Dtos;
using DummyGames.API.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace DummyGames.API.Controllers
{
	[ServiceFilter(typeof(LogUserActivity))]
	[Route("api/[controller]")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		private readonly IRepository repo;
		private readonly IMapper mapper;

		public UsersController(IRepository repo, IMapper mapper)
		{
			this.repo = repo;
			this.mapper = mapper;
		}

		[HttpGet]
		public async Task<IActionResult> GetUsers([FromQuery] UserParams userParams)
		{
			userParams.SenderId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

			var users = await repo.GetUsers(userParams);

			var usersToReturn = mapper.Map<IEnumerable<UserForDetailedDto>>(users);

			Response.AddPagination(users.CurrentPage, users.PageSize, users.TotalCount, users.TotalPages);

			return Ok(usersToReturn);
		}

		[HttpGet("{id}", Name = "GetUser")]
		public async Task<IActionResult> GetUser(int id)
		{
			var user = await repo.GetUser(id);

			var userToReturn = mapper.Map<UserForDetailedDto>(user);

			return Ok(userToReturn);
		}
	}
}