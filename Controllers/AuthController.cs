using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DummyGames.API.Data;
using DummyGames.API.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DummyGames.API.Controllers
{
	[AllowAnonymous]
	[Route("api/[controller]")]
	[ApiController]
	public class AuthController : ControllerBase
	{
		const int tokenExpirationTime = 60;	// in minutes
		private readonly IMapper mapper;
		private readonly UserManager<User> userManager;
		private readonly SignInManager<User> signInManager;
		private readonly IConfiguration config;

		public AuthController(IMapper mapper, UserManager<User> userManager,
		SignInManager<User> signInManager, IConfiguration config)
		{
			this.mapper = mapper;
			this.userManager = userManager;
			this.signInManager = signInManager;
			this.config = config;
		}

		[HttpPost("register")]
		public async Task<IActionResult> Register(UserForRegisterDto userForRegisterDto)
		{
			var userToCreate = mapper.Map<User>(userForRegisterDto);

			var result = await userManager.CreateAsync(userToCreate, userForRegisterDto.Password);

			if (!result.Succeeded)
				return BadRequest(result.Errors);

			var userToReturn = mapper.Map<UserForDetailedDto>(userToCreate);

			return CreatedAtRoute("GetUser", new { Controller = "Users", id = userToReturn.Id }, userToReturn);
		}

		[HttpPost("login")]
		public async Task<IActionResult> Login(UserForLoginDto userForLoginDto)
		{
			var emailValidator = new EmailAddressAttribute();
			User user;

			if (emailValidator.IsValid(userForLoginDto.Identifier))
				user = await userManager.FindByEmailAsync(userForLoginDto.Identifier);
			else
				user = await userManager.FindByNameAsync(userForLoginDto.Identifier);

			if(user == null)
				return NotFound();

			if (!(await signInManager.CheckPasswordSignInAsync(user, userForLoginDto.Password, true)).Succeeded)
				return Unauthorized();

			// var appUser = await userManager.Users
			// 	.FirstOrDefaultAsync(u => u.NormalizedUserName == user.NormalizedUserName);

			var userToReturn = mapper.Map<UserForDetailedDto>(user);

			return Ok(new
			{
				token = GenerateJwtToken(user).Result,
				user = userToReturn
			});
		}

		private async Task<string> GenerateJwtToken(User user)
		{
			var claims = new List<Claim>
			{
				new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
				new Claim(ClaimTypes.Name, user.UserName)
			};

			var roles = await userManager.GetRolesAsync(user);

			foreach (var role in roles)
			{
				claims.Add(new Claim(ClaimTypes.Role, role));
			}

			var key = new SymmetricSecurityKey(Encoding.UTF8
				.GetBytes(config.GetSection("AppSettings:Token").Value));

			var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Subject = new ClaimsIdentity(claims),
				Expires = DateTime.Now.AddMinutes(tokenExpirationTime),
				SigningCredentials = creds
			};

			var tokenHandler = new JwtSecurityTokenHandler();

			var token = tokenHandler.CreateToken(tokenDescriptor);

			return tokenHandler.WriteToken(token);
		}
	}
}