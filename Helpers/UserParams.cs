using System.Collections.Generic;

namespace DummyGames.API.Data
{
	public class UserParams
	{
		private const int MaxPageSize = 50;
		private int pageSize = 10;
		public int PageNumber { get; set; } = 1;
		public int SenderId { get; set; }
		public ICollection<int> userIds { get; set; }
		public string OrderBy { get; set; }	// TODO: Change OrderBy to enum
		public int PageSize
		{
			get { return pageSize; }
			set { pageSize = (value > MaxPageSize) ? MaxPageSize : value; }
		}
	}
}