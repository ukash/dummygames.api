using AutoMapper;
using DummyGames.API.Data;
using DummyGames.API.Dtos;
using DummyGames.API.Models;

namespace DummyGames.API.Helpers
{
	public class AutoMapperProfiles : Profile
	{
		public AutoMapperProfiles()
		{
			CreateMap<UserForRegisterDto, User>();
			CreateMap<ChatMessageDto, ChatMessage>();
			CreateMap<GameRequestDto, GameRequest>();
			CreateMap<GameStateDto, Game>();
		}
	}
}