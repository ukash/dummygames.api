using System.Linq;
using System.Threading.Tasks;
using DummyGames.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using DummyGames.API.Models;
using DummyGames.API.Data;
using System.Collections.Generic;
using AutoMapper;
using DummyGames.API.Dtos;
using System.Drawing;

namespace DummyGames.API.Hubs
{
	[Authorize]
	public class GameHub : Hub<IGameClient>
	{
		private readonly IOnlineClientsService onlineClientsService;
		private readonly IGameService gameService;
		private readonly IRepository repo;
		private readonly IMapper mapper;

		public GameHub(IOnlineClientsService onlineClientsService, IGameService gameService,
			IRepository repo, IMapper mapper)
		{
			this.onlineClientsService = onlineClientsService;
			this.gameService = gameService;
			this.repo = repo;
			this.mapper = mapper;
		}

		public async Task SendGameMove(int gameId, dynamic move)
		{
			try
			{
				if (!gameService.IsUserTurn(int.Parse(Context.UserIdentifier), gameId))
					throw new HubException("It is not your turn yet.");

				await gameService.GameMove(gameId, move);
			}
			catch (System.Exception e) { throw new HubException(e.Message); }
		}

		public async Task Challenge(int userId, int bid, GameType gameType)
		{
			if (int.Parse(Context.UserIdentifier) == userId)
				throw new HubException("You cannot challenge yourself.");

			if (!onlineClientsService.OnlineUsers.userIds.Contains(userId))
				throw new HubException("User is offline.");

			//	TODO: Uncomment when implemented
			// if (gameService.InGamePlayers.Contains(userId))
			// 	throw new HubException("User is currently in game.");

			//	TODO: Check whether sender has already sent a game request to the recipient.

			if (!(await repo.GetUser(int.Parse(Context.UserIdentifier)) is User sender))
				throw new HubException("Failed to retrieve your data.");

			if (!(await repo.GetUser(userId) is User recipient))
				throw new HubException("Failed to retrieve your opponent data.");

			var gameRequest = new GameRequest(sender, recipient, bid, gameType);

			repo.Add(gameRequest);

			if (await repo.SaveAll())
			{
				gameService.AddGameRequest(gameRequest);

				var gameRequestToReturn = mapper.Map<GameRequestDto>(gameRequest);
				await Clients.Users(sender.Id.ToString(), recipient.Id.ToString()).ReceiveChallenge(gameRequestToReturn);
			}
			else
				throw new HubException("Failed to create a new game request.");
		}

		public async Task UpdateGameRequest(int id, GameRequestStatus status)
		{
			if (!(gameService.InProgressGameRequests.SingleOrDefault(r => r.Id == id) is GameRequest gameRequest))
				throw new HubException("This game request is no longer valid.");

			var senderId = int.Parse(Context.UserIdentifier);

			switch (status)
			{
				case GameRequestStatus.Accepted:
				case GameRequestStatus.Rejected:
					if (gameRequest.RecipientId != senderId)
						throw new HubException("You are not the recipient of this game request.");
					break;

				case GameRequestStatus.Cancelled:
					if (gameRequest.SenderId != senderId)
						throw new HubException("You can only cancel the game requests you have sent by yourself.");
					break;

				default:
					throw new HubException("Invalid operation.");
			}

			try
			{
				var updatedGameRequest = await gameService.UpdateInProgressGameRequest(id, status);

				repo.Update(updatedGameRequest);

				if (await repo.SaveAll())
				{
					var gameRequestToReturn = mapper.Map<GameRequestDto>(updatedGameRequest);
					await Clients.Users(gameRequest.SenderId.ToString(), gameRequest.RecipientId.ToString())
						.ReceiveChallenge(gameRequestToReturn);
				}
				else throw new HubException("Failed to save updated game request.");
			}
			catch (System.Exception e) { throw new HubException(e.Message); }
		}
	}
}