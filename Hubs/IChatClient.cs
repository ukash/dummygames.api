using System.Collections.Generic;
using System.Threading.Tasks;
using DummyGames.API.Dtos;

namespace DummyGames.API.Hubs
{
	public interface IChatClient
	{
		Task ReceiveMessage(ChatMessageDto message);
		Task ReceiveMessages(IEnumerable<ChatMessageDto> messages);
		Task ReceiveOnlineUsers(OnlineUsersDto onlineUsers);
	}
}