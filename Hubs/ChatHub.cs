using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using AutoMapper;
using DummyGames.API.Data;
using DummyGames.API.Dtos;
using DummyGames.API.Models;
using DummyGames.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;

namespace DummyGames.API.Hubs
{
	public class ChatHub : Hub<IChatClient>
	{
		private readonly IRepository repo;
		private readonly IMapper mapper;
		private readonly IChatService chatService;
		private readonly IOnlineClientsService onlineClientsService;

		public ChatHub(IRepository repo, IMapper mapper, IChatService chatService,
			IOnlineClientsService onlineClientsService)
		{
			this.repo = repo;
			this.mapper = mapper;
			this.chatService = chatService;
			this.onlineClientsService = onlineClientsService;
		}

		[Authorize]
		public async Task SendMessage(string message)
		{
			if (!(await repo.GetUser(int.Parse(Context.UserIdentifier)) is User sender))
				throw new HubException("Failed to retrieve your data.");

			var messageToAdd = new ChatMessage(sender, message);

			try
			{
				var newMessage = await AddMessage(messageToAdd);
				var messageToReturn = mapper.Map<ChatMessageDto>(newMessage);
				await Clients.All.ReceiveMessage(messageToReturn);
			}
			catch (System.Exception e) { throw new HubException(e.Message); }
		}

		private async Task<ChatMessage> AddMessage(ChatMessage message)
		{
			repo.Add(message);

			if (await repo.SaveAll())
			{
				chatService.AddMessage(message);
				return message;
			}
			throw new System.Exception("Adding new chat message failed on save.");
		}

		public async Task GetMessages()
		{
			var messagesToReturn = mapper.Map<IEnumerable<ChatMessageDto>>(chatService.Messages);
			await Clients.Caller.ReceiveMessages(messagesToReturn);
		}

		public async Task GetOnlineUsers()
			=> await Clients.Caller.ReceiveOnlineUsers(onlineClientsService.OnlineUsers);
		public override async Task OnConnectedAsync()
		{
			onlineClientsService.AddClient(Context.ConnectionId, Context.UserIdentifier);
			await base.OnConnectedAsync();
		}

		public override async Task OnDisconnectedAsync(Exception exception)
		{
			onlineClientsService.RemoveClient(Context.ConnectionId);
			await base.OnDisconnectedAsync(exception);
		}
	}
}