using System.Threading.Tasks;
using DummyGames.API.Dtos;
using DummyGames.API.Models;

namespace DummyGames.API.Hubs
{
    public interface IGameClient
    {
         Task ReceiveChallenge(GameRequestDto gameRequestDto);
         Task ReceiveGameState(GameStateDto gameStateDto);
    }
}